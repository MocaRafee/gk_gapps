#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2020 Raphielscape LLC. and Haruka LLC.
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

# Recorder
ifeq ($(TARGET_SUPPORTS_GOOGLE_RECORDER), true)
PRODUCT_PACKAGES += \
    RecorderPrebuilt
endif

# arcore
ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
PRODUCT_PACKAGES += \
    arcore
endif

# Live Wallpapers
ifeq ($(TARGET_INCLUDE_LIVE_WALLPAPERS),true)
PRODUCT_PACKAGES += \
    MicropaperPrebuilt \
    WallpapersBReel2020 \
    PixelLiveWallpaperPrebuilt \
    PixelWallpapers2020
endif

# Default apps, you want more? see below
# product/app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    DevicePolicyPrebuilt \
    GalleryGo \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    MarkupGoogle \
    PixelThemesStub \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    SoundPickerPrebuilt

# product/priv-app
PRODUCT_PACKAGES += \
    AmbientSensePrebuilt \
    AndroidMigratePrebuilt \
    GoogleDialer \
    GoogleOneTimeInitializer \
    FilesPrebuilt \
    DevicePersonalizationPrebuiltPixel2020 \
    Phonesky \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizardPrebuilt \
    TetheringEntitlement \
    TurboPrebuilt \
    WellbeingPrebuilt \
    WfcActivation

# system/app
PRODUCT_PACKAGES += \
    GoogleExtShared

# system/priv-app
PRODUCT_PACKAGES += \
    DocumentsUIGoogle

# system_ext/app
PRODUCT_PACKAGES += \
    Flipendo

# system_ext/priv-app
PRODUCT_PACKAGES += \
    GoogleOneTimeInitializer \
    GoogleServicesFramework \
    grilservice \
    NexusLauncherRelease \
    PixelSetupWizard \
    RilConfigService \
    StorageManagerGoogle \
    TurboAdapter \
    WallpaperPickerGoogleRelease

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCoreRvc \
    PrebuiltGmsCoreRvc_AdsDynamite \
    PrebuiltGmsCoreRvc_CronetDynamite \
    PrebuiltGmsCoreRvc_DynamiteLoader \
    PrebuiltGmsCoreRvc_DynamiteModulesA \
    PrebuiltGmsCoreRvc_DynamiteModulesC \
    PrebuiltGmsCoreRvc_GoogleCertificates \
    PrebuiltGmsCoreRvc_MapsDynamite \
    PrebuiltGmsCoreRvc_MeasurementDynamite \
    AndroidPlatformServices

PRODUCT_PACKAGES += \
    libprotobuf-cpp-full \
    librsjni

# Cuz we're only shipped with basic, not bloated type, but you want more? there you go
ifeq ($(EXTRA_GAPPS), true)
# product/app
PRODUCT_PACKAGES += \
    CarrierMetrics \
    Chrome \
    Chrome-Stub \
    DiagnosticsToolPrebuilt \
    GoogleCamera \
    GoogleTTS \
    Ornament \
    Photos \
    PrebuiltGoogleTelemetryTvp \
    SoundAmplifierPrebuilt \
    TrichromeLibrary \
    TrichromeLibrary-Stub \
    Tycho \
    talkback \
    WebViewGoogle \
    WebViewGoogle-Stub \
    Tycho

# product/priv-app
PRODUCT_PACKAGES += \
    AndroidAutoStubPrebuilt \
    CarrierLocation \
    CarrierServices \
    CarrierWifi \
    ConfigUpdater \
    ConnMetrics \
    DMService \
    MaestroPrebuilt \
    NovaBugreportWrapper \
    PartnerSetupPrebuilt \
    SCONE \
    ScribePrebuilt \
    SafetyHubPrebuilt \
    Velvet

# system/app
PRODUCT_PACKAGES += \
    GooglePrintRecommendationService

# system/priv-app
PRODUCT_PACKAGES += \
    GooglePackageInstaller \
    TagGoogle

# system_ext/priv-app
PRODUCT_PACKAGES += \
    CarrierSetup \
    CbrsNetworkMonitor \
    GoogleFeedback
endif

$(call inherit-product, vendor/gapps/product/blobs/product_blobs.mk)
$(call inherit-product, vendor/gapps/system/blobs/system_blobs.mk)
$(call inherit-product, vendor/gapps/system_ext/blobs/system-ext_blobs.mk)

